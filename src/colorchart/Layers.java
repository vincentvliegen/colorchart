package colorchart;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.*;

import colorchart.components.BorderPanel;
import colorchart.components.ColorPanel;
import colorchart.components.TextPanel;
import colorchart.components.sliders.IntensitySlider;
import colorchart.mouseListener.HexagonResponse;
import colorchart.mouseListener.HexagonSelector;
import colorchart.mouseListener.IntensitySliderResponse;
import colorchart.mouseListener.LuminanceChangeListener;
import colorchart.mouseListener.MouseListener;
import colorchart.mouseListener.PrecisionChangeListener;
import colorchart.mouseListener.SliderListener;
import colorchart.shapes.Triangle;

public class Layers extends JLayeredPane {

	private static final long serialVersionUID = 1L;
	private int maxColorValue;
	private final static int heightBorder = 750;
	private final static int margin = 40;
	private final IntensitySlider intensitySlider;
//	private final JSlider luminanceSlider;
	private final JSlider precisionSlider;
	private final ColorPanel colorPanel;
	private final BorderPanel borderPanel;
	private JLabel backgroundPanel;
	private final TextPanel RGBTextPanel;
	private final TextPanel HSVTextPanel;
	private final TextPanel HSLTextPanel;
	private final TextPanel HSITextPanel;

	public Layers() {
		try {
			BufferedImage picture = ImageIO.read(getClass().getResource("/images/dark_concrete.png"));
			setBackgroundPanel(new JLabel(new ImageIcon(scaleBackground(picture))));
			getBackgroundPanel().setSize(getBackgroundPanel().getIcon().getIconWidth(),
					getBackgroundPanel().getIcon().getIconHeight());
			add(getBackgroundPanel(), new Integer(0));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		setMaxColorValue(PrecisionChangeListener.getPrecisionlist()[PrecisionChangeListener.getPrecisionlist().length/2]);

		this.colorPanel = new ColorPanel(getMaxColorValue() * 3 / 2, this);
		getColorPanel().setSize(getWidthPanel(),getHeightPanel());
		add(getColorPanel(), new Integer(1));

		this.borderPanel = new BorderPanel(this);
		getBorderPanel().setSize(getWidthPanel(),getHeightPanel());
		add(getBorderPanel(), new Integer(2));

		this.RGBTextPanel = createTextPanel("RGB: ");
		add(getRGBTextPanel(), new Integer(3));
		
		this.HSVTextPanel = createTextPanel("HSV: ");
		add(getHSVTextPanel(), new Integer(3));
		
		this.HSLTextPanel = createTextPanel("HSL: ");
		add(getHSLTextPanel(), new Integer(3));

		this.HSITextPanel = createTextPanel("HSI: ");
		add(getHSITextPanel(), new Integer(3));
		
//		this.luminanceSlider = createLuminanceSlider();
//		add(getLuminanceSlider(), new Integer(3));

		this.intensitySlider = createIntensitySlider();
		add(getIntensitySlider(), new Integer(3));
		
		this.precisionSlider = createPrecisionSlider();
		add(getPrecisionSlider(), new Integer(3));

		MouseListener listener = new MouseListener(this);
		listener.getResponses().add(new HexagonResponse(this));
		listener.getResponses().add(new IntensitySliderResponse(this));
		this.addMouseListener(listener);
		this.addMouseMotionListener(listener);

	}

	public IntensitySlider createIntensitySlider(){
		int width = getBorderPanel().getTriangleWidth();
		int height = getBorderPanel().getTriangleHeight();
		Point botLeft = new Point(0,height);
		Point topLeft = new Point(0,0);
		Point botRight = new Point(width,height);
//		Point botLeft = new Point(getMargin(), getMargin() + getHeightborder());
//		Point topLeft = new Point(botLeft.x, botLeft.y - getBorderPanel().getTriangleHeight());
//		Point botRight = new Point(botLeft.x + getBorderPanel().getTriangleWidth(), botLeft.y);
		Triangle zone = new Triangle(botLeft, botRight, topLeft);
		IntensitySlider slider = new IntensitySlider(0, getMaxColorValue()*3, zone, this);
		slider.setSize(width+1, height+1);
		return slider;
	}
	
//	public JSlider createLuminanceSlider() {
//		JSlider slider = new JSlider();
//		slider.setMinimum(-getMaxColorValue() * 3);
//		slider.setMaximum(0);
//		slider.setValue(-getColorPanel().getImageNumber());
//		slider.setPaintTrack(false);
//		slider.setSize(new Dimension(getBorderPanel().getTriangleWidth() + 1, getBorderPanel().getTriangleHeight()));
//		slider.setOpaque(false);
//		slider.addChangeListener(new LuminanceChangeListener());
//		return slider;
//	}

	public JSlider createPrecisionSlider() {
		JSlider slider = new JSlider();
		slider.setMinimum(0);
		slider.setMaximum(PrecisionChangeListener.getPrecisionlist().length-1);
		slider.setValue(slider.getMaximum()/2);
		slider.setPaintTrack(false);
		slider.setSize(new Dimension(getBorderPanel().getTriangleWidth() + 1, getBorderPanel().getTriangleHeight()));
		slider.setOpaque(false);
		slider.addChangeListener(new PrecisionChangeListener());
		return slider;
	}

	public TextPanel createTextPanel(String label) {
		TextPanel textPanel = new TextPanel(this,label);
		textPanel.setSize(90,20);
		return textPanel;
	}

	@Override
    public Dimension getPreferredSize() {
        return new Dimension(getWidthPanel(),getHeightPanel());
    }
	
	@Override
	public void doLayout() {
		super.doLayout();

		int x;
		int xColorPanel;
		int yColorPanel;
		int y;
		int width;
		int height;

		// colorPanel = centered
		width = getWidthPanel();
		height = getHeightPanel();
		xColorPanel = (int) ((getWidth() - width) / 2);
		x = xColorPanel;
		yColorPanel = (int) ((getHeight() - height) / 2);
		y = yColorPanel;
		getColorPanel().setBounds(x, y, width, height);

		// borderPanel = centered = colorpanel
		getBorderPanel().setBounds(x, y, width, height);

//		// luminanceSlider = bottom left
//		width = getLuminanceSlider().getWidth();
//		height = getLuminanceSlider().getHeight();
//		x = xColorPanel + getMargin();
//		y = yColorPanel + getColorPanel().getHeight() - getMargin() - height / 2;
//		getLuminanceSlider().setBounds(x, y, width, height);

		//intensitySlider = bottom left
		width = getIntensitySlider().getWidth();
		height = getIntensitySlider().getHeight();
		x = xColorPanel + getMargin();
		y = yColorPanel + getHeightPanel() - getMargin() - height;
		getIntensitySlider().setBounds(x, y, width, height);
		
		// precisionSlider = bottom right
		width = getPrecisionSlider().getWidth();
		height = getPrecisionSlider().getHeight();
		x = xColorPanel + getWidthPanel() - getMargin() - width + 1;
		y = yColorPanel + getHeightPanel() - getMargin() - height / 2;
		getPrecisionSlider().setBounds(x, y, width, height);

		// rgb text = top right
		width = getRGBTextPanel().getWidth();
		height = getRGBTextPanel().getHeight();
		x = xColorPanel + getWidthPanel() - getMargin() - width -1 - 5;
		y = yColorPanel + getMargin() + 5;
		getRGBTextPanel().setBounds(x, y, width, height);
		
		// hsv text = top right below rgb
		
		width = getHSVTextPanel().getWidth();
		height = getHSVTextPanel().getHeight();
		y += 5 + getRGBTextPanel().getHeight();
		getHSVTextPanel().setBounds(x, y, width, height);
		
		// hsl text = top right below hsv
		
		width = getHSLTextPanel().getWidth();
		height = getHSLTextPanel().getHeight();
		y += 5 + getHSVTextPanel().getHeight();
		getHSLTextPanel().setBounds(x, y, width, height);
		
		// hsl text = top right below hsv
		
		width = getHSITextPanel().getWidth();
		height = getHSITextPanel().getHeight();
		y += 5 + getHSLTextPanel().getHeight();
		getHSITextPanel().setBounds(x, y, width, height);

	}
	
	public Image scaleBackground(BufferedImage backGround) {
		int height;
		int width;
		Toolkit toolkit = Toolkit.getDefaultToolkit();

		Dimension screenSize = toolkit.getScreenSize();
		// System.out.println("Physical screen size: " + screenSize);

		Insets insets = toolkit.getScreenInsets(
				GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDefaultConfiguration());
		// System.out.println("Insets: " + insets);

		screenSize.width -= (insets.left + insets.right);
		screenSize.height -= (insets.top + insets.bottom);
		// System.out.println("Max available: " + screenSize);

		// avoid warping
		double ratioImage = backGround.getHeight() / (double) (backGround.getWidth());
		double ratioScreen = screenSize.getHeight() / screenSize.getWidth();
		if (ratioImage < ratioScreen) {// width too small
			height = screenSize.height;
			width = (int) (height / ratioImage);
		} else {// height too small
			width = screenSize.width;
			height = (int) (width * ratioImage);
		}

		Image scaledBackGround = backGround.getScaledInstance(width, height, Image.SCALE_SMOOTH);
		return scaledBackGround;
	}

	
	
	public ColorPanel getColorPanel() {
		return colorPanel;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

//	public JSlider getLuminanceSlider() {
//		return luminanceSlider;
//	}

	public JSlider getPrecisionSlider() {
		return precisionSlider;
	}

	public JLabel getBackgroundPanel() {
		return backgroundPanel;
	}

	public void setBackgroundPanel(JLabel backgroundPanel) {
		this.backgroundPanel = backgroundPanel;
	}
	
	public static int getHeightborder() {
		return heightBorder;
	}

	public static int getWidthborder() {
		return (int) Math.round(getHeightborder() * Math.cos(Math.PI / 6.0));
	}

	public static int getMargin() {
		return margin;
	}

	public int getMaxColorValue() {
		return maxColorValue;
	}

	public void setMaxColorValue(int maxColorValue) {
		this.maxColorValue = maxColorValue;
	}

	public BorderPanel getBorderPanel() {
		return borderPanel;
	}

	public static int getWidthPanel(){
		return getWidthborder() + 2 * getMargin();
	}
	
	public static int getHeightPanel(){
		return getHeightborder() + 2 * getMargin();
	}


	public TextPanel getRGBTextPanel() {
		return RGBTextPanel;
	}

	public TextPanel getHSVTextPanel() {
		return HSVTextPanel;
	}

	public TextPanel getHSLTextPanel() {
		return HSLTextPanel;
	}


	public TextPanel getHSITextPanel() {
		return HSITextPanel;
	}

	public IntensitySlider getIntensitySlider() {
		return intensitySlider;
	}
		
	
	
}