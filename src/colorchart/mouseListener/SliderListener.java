package colorchart.mouseListener;

import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import colorchart.components.sliders.TriangularSlider;

public class SliderListener extends MouseAdapter{

	private final TriangularSlider slider;
	
	public SliderListener(TriangularSlider slider) {
		this.slider = slider;
	}

	public void mouseClicked(MouseEvent event) {//TODO click to increase little bit (if left then left 1 value)
		System.out.println("slider");
		Point point = event.getPoint();
		double xdif = getSlider().getLayers().getX();
		double ydif = getSlider().getLayers().getY();
		point.translate((int) xdif, (int) ydif);
		if (getSlider().getZone().contains(point)) {
			int newValue;
			double x = point.getX();
			double xMax = getSlider().getMaxTrack()[0];
			double xMin = getSlider().getMinTrack()[0];
			int vMax = getSlider().getMaxValue();
			int vMin = getSlider().getMinValue();
			if((x < xMax && x > xMin)||(x < xMin && x > xMax)){//inbetween xMin and xMax
				double ratio = (x - xMin)/(xMax - xMin);
				newValue = (int) (ratio*(vMax - vMin) + vMin);
			}else if((xMax >= x && xMax > xMin)||(xMax <= x && xMax <= xMin)){// x = xMax
				newValue = vMax;
			}else{// x = xMin
				newValue = vMin;
			}
			
			getSlider().setValue(newValue);
			getSlider().calculateButton();
			getSlider().updateImage();
		}
	}

	public void mouseDragged(MouseEvent event) {
		mouseClicked(event);
	}

	
	
	public TriangularSlider getSlider() {
		return slider;
	}
	
}
