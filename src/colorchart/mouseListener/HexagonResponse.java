package colorchart.mouseListener;

import java.awt.Color;
import java.awt.Point;
import java.util.ArrayList;

import colorchart.Layers;
import colorchart.colors.ColorConversions;
import colorchart.shapes.Hexagon;
import colorchart.shapes.Shape;

public class HexagonResponse extends MouseResponse{

	private Color currentColor;

	
	public HexagonResponse(Layers layers) {
		super(layers, layers.getBorderPanel().getListOfShapes().get(0));
	}

	@Override
	public void clickResponse(Point point) {
//		System.out.println("in hexagon zone");
		ArrayList<Shape> hexes = getLayers().getColorPanel().getListOfShapes();
		ArrayList<Shape> allShapes = getLayers().getBorderPanel().getListOfShapes();
		for (Shape hex : hexes) {
			if (hex.contains(point)) {
				if (getCurrentColor() != hex.getFillColor()) {
					Shape s = allShapes.get(allShapes.size() - 1);
					if (s instanceof Hexagon) {
						allShapes.remove(s);
					}
					allShapes.add(hex);
					allShapes.get(allShapes.size() - 1).setBorderColor(new Color(0, 0, 0));
					Color color = hex.getFillColor();
					allShapes.get(1).setFillColor(color);// top left triangle
					setCurrentColor(color);
					getLayers().getBorderPanel().updateImage();
					getLayers().getRGBTextPanel().getTextField().setText(ColorConversions.ColorToHexRGB(color));
					getLayers().getHSVTextPanel().getTextField().setText(ColorConversions.ColorToHSV(color));
					getLayers().getHSLTextPanel().getTextField().setText(ColorConversions.ColorToHSL(color));
					getLayers().getHSITextPanel().getTextField().setText(ColorConversions.ColorToHSI(color));
				}
				break;
			}
		}		
	}

	@Override
	public void dragResponse(Point point) {
		clickResponse(point);	
	}

	public Color getCurrentColor() {
		return currentColor;
	}

	public void setCurrentColor(Color currentColor) {
		this.currentColor = currentColor;
	}
	
}
