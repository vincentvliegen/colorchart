package colorchart.mouseListener;

import java.awt.Point;

import colorchart.Layers;
import colorchart.shapes.Shape;

public abstract class MouseResponse {

	private final Layers layers;
	private final Shape area;
	
	public MouseResponse(Layers layers, Shape area) {
		this.layers = layers;
		this.area = area;
	}

	public abstract void clickResponse(Point point);
	
	public abstract void dragResponse(Point point);
	
	public Layers getLayers() {
		return layers;
	}

	public Shape getArea(){
		return area;
	}
}
