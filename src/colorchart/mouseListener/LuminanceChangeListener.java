package colorchart.mouseListener;

import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import colorchart.Layers;

public class LuminanceChangeListener implements ChangeListener {

	private int previousValue;

	public LuminanceChangeListener() {

	}

	@Override
	public void stateChanged(ChangeEvent e) {
		JSlider source = (JSlider) e.getSource();
		int panelNumber = (int) -source.getValue();
		if (panelNumber != getPreviousValue()) {
			Layers layers = (Layers) source.getParent();
			layers.getColorPanel().changeImage(panelNumber);
			layers.getBorderPanel().removePointer();
			setPreviousValue(panelNumber);
		}
	}

	public int getPreviousValue() {
		return previousValue;
	}

	public void setPreviousValue(int value) {
		this.previousValue = value;
	}

}
