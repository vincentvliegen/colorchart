package colorchart.mouseListener;

import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import colorchart.Layers;

public class PrecisionChangeListener implements ChangeListener {

	private final static int[] precisionList = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 12, 14, 16, 18, 20, 26, 32, 40, 48, 56,
			64 };
	private int previousValue;

	public PrecisionChangeListener() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void stateChanged(ChangeEvent e) {
		JSlider source = (JSlider) e.getSource();
		int index = source.getValue();
		int maxColorValue = PrecisionChangeListener.getPrecisionlist()[index];
		if (maxColorValue != getPreviousValue()) {
			Layers layers = (Layers) source.getParent();
			double previousColorValue = layers.getMaxColorValue();
			int previousPanelNumber = layers.getColorPanel().getImageNumber();
			layers.setMaxColorValue((int) maxColorValue);
			int newPanelNumber = (int) Math.round(previousPanelNumber * (maxColorValue * 3) / (previousColorValue * 3));
			layers.getColorPanel().changeImage(newPanelNumber);
			layers.getBorderPanel().removePointer();
//			layers.getLuminanceSlider().setMinimum(-3 * maxColorValue);
//			layers.getLuminanceSlider().setValue(-newPanelNumber);
			layers.getIntensitySlider().setMaxValue(3*maxColorValue);
			layers.getIntensitySlider().setValue(newPanelNumber);
			setPreviousValue(maxColorValue);
		}
	}

	public static int[] getPrecisionlist() {
		return precisionList;
	}

	public int getPreviousValue() {
		return previousValue;
	}

	public void setPreviousValue(int value) {
		this.previousValue = value;
	}

}
