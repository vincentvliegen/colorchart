package colorchart.mouseListener;

import java.awt.Point;

import colorchart.Layers;
import colorchart.components.sliders.IntensitySlider;
import colorchart.shapes.Shape;
import colorchart.shapes.Triangle;

public class IntensitySliderResponse extends MouseResponse{

	private final IntensitySlider slider; 
	private final Triangle zone;
	
	public IntensitySliderResponse(Layers layers) {
		super(layers, layers.getIntensitySlider().getZone());
		this.slider = getLayers().getIntensitySlider();
		this.zone = layers.getIntensitySlider().getZone();
	}

	@Override
	public void clickResponse(Point point) {
//		System.out.println("slider");
		int newValue;
		double x = point.getX();
		x -= Layers.getMargin();//corrected
		double xMax = getSlider().getMaxTrack()[0];
		double xMin = getSlider().getMinTrack()[0];
		int vMax = getSlider().getMaxValue();
		int vMin = getSlider().getMinValue();
		if((x < xMax && x > xMin)||(x < xMin && x > xMax)){//inbetween xMin and xMax
			double ratio = (x - xMin)/(xMax - xMin);
			newValue = (int) (ratio*(vMax - vMin) + vMin);
		}else if((xMax <= x && xMax > xMin)||(xMax >= x && xMax <= xMin)){// x = xMax
			newValue = vMax;
		}else{// x = xMin
			newValue = vMin;
		}
		getLayers().getColorPanel().changeImage(newValue);
		getLayers().getBorderPanel().removePointer();
		getSlider().updateButton(newValue);
		getSlider().updateImage();
	}

	@Override
	public void dragResponse(Point point) {
		clickResponse(point);
	}

	public Triangle translatedZone(){
		Point p0 = new Point();
		Point p1 = new Point();
		Point p2 = new Point();
		Triangle newTriangle;
		double x = Layers.getMargin();
		double y = Layers.getHeightPanel() - Layers.getMargin() - getLayers().getIntensitySlider().getHeight();
		
		p0.setLocation(x + getZone().xpoints[0], y + getZone().ypoints[0]);
		p1.setLocation(x + getZone().xpoints[1], y + getZone().ypoints[1]);
		p2.setLocation(x + getZone().xpoints[2], y + getZone().ypoints[2]);
		newTriangle = new Triangle(p0,p1,p2);
		return newTriangle;
	}
	
	public IntensitySlider getSlider() {
		return slider;
	}

	@Override
	public Shape getArea() {
		return translatedZone();
	}

	public Triangle getZone() {
		return zone;
	}
	
	
}
