package colorchart.mouseListener;

import java.awt.*;
import java.awt.event.*;
import java.util.*;

import colorchart.Layers;
import colorchart.colors.ColorConversions;
import colorchart.shapes.Hexagon;
import colorchart.shapes.Shape;

public class HexagonSelector extends MouseAdapter {

	private final Layers layers;
	private Color currentColor;

	public HexagonSelector(Layers layers) {
		super();
		this.layers = layers;
	}

	public void mouseClicked(MouseEvent event) {
		System.out.println("hexagonselector");
		Point point = event.getPoint();
		point.translate(-getLayers().getColorPanel().getX(), -getLayers().getColorPanel().getY());
		ArrayList<Shape> hexes = getLayers().getColorPanel().getListOfShapes();
		ArrayList<Shape> allShapes = getLayers().getBorderPanel().getListOfShapes();
		for (Shape hex : hexes) {
			if (hex.contains(point)) {
				if (getCurrentColor() != hex.getFillColor()) {
					Shape s = allShapes.get(allShapes.size() - 1);
					if (s instanceof Hexagon) {
						allShapes.remove(s);
					}
					allShapes.add(hex);
					allShapes.get(allShapes.size() - 1).setBorderColor(new Color(0, 0, 0));
					Color color = hex.getFillColor();
					allShapes.get(1).setFillColor(color);// top left triangle
					setCurrentColor(color);
					getLayers().getBorderPanel().updateImage();
					getLayers().getRGBTextPanel().getTextField().setText(ColorConversions.ColorToHexRGB(color));
					getLayers().getHSVTextPanel().getTextField().setText(ColorConversions.ColorToHSV(color));
					getLayers().getHSLTextPanel().getTextField().setText(ColorConversions.ColorToHSL(color));
					getLayers().getHSITextPanel().getTextField().setText(ColorConversions.ColorToHSI(color));
				}
				break;
			}
		}
	}

	public void mouseDragged(MouseEvent event) {
		mouseClicked(event);
	}

	public Color getCurrentColor() {
		return currentColor;
	}

	public void setCurrentColor(Color currentColor) {
		this.currentColor = currentColor;
	}

	public Layers getLayers() {
		return layers;
	}

	
	
}
