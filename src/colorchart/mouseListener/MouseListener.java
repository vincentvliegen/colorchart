package colorchart.mouseListener;

import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import colorchart.Layers;

public class MouseListener extends MouseAdapter{

	private final Layers layers;
	private final ArrayList<MouseResponse> responses;

 	public MouseListener(Layers layers) {
		super();
		this.layers = layers;
		responses = new ArrayList<MouseResponse>();
	}

	public void mouseClicked(MouseEvent event) {
		Point point = event.getPoint();
		point.translate(-getLayers().getColorPanel().getX(), -getLayers().getColorPanel().getY());
//		System.out.println("point clicked: [" + point.getX() + ", " + point.getY() + "]");
		for(MouseResponse r : getResponses()){
			if(r.getArea().contains(point)){
				r.clickResponse(point);
			}
		}
	}

	public void mouseDragged(MouseEvent event) {
		Point point = event.getPoint();
		point.translate(-getLayers().getColorPanel().getX(), -getLayers().getColorPanel().getY());
//		System.out.println("point dragged: [" + point.getX() + ", " + point.getY() + "]");
		for(MouseResponse r : getResponses()){
			if(r.getArea().contains(point)){
				r.dragResponse(point);
			}
		}
	}
	
	public Layers getLayers() {
		return layers;
	}


	public ArrayList<MouseResponse> getResponses() {
		return responses;
	}
	
}
