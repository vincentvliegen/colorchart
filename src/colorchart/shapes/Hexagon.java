package colorchart.shapes;

import java.awt.*;

public class Hexagon extends Shape {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3135235734809759978L;

	public Hexagon(double[] topPoint, double heightHexagon, Color fillColor, Color borderColor) {
		super(fillColor, borderColor);
		createHexagon(topPoint, heightHexagon);
	}

	public Hexagon(double[] topPoint, double heightHexagon) {
		super();
		createHexagon(topPoint, heightHexagon);
	}
	
	private void createHexagon(double[] topPoint, double heightHexagon) {

		double widthHexagon = heightHexagon * Math.cos(Math.PI / 6.0);

		// generate points
		Point top = new Point((int) Math.round(topPoint[0]), (int) Math.round(topPoint[1]));
		Point bot = new Point((int) Math.round(topPoint[0]), (int) Math.round(topPoint[1] + heightHexagon));
		Point topLeft = new Point((int) Math.round(topPoint[0] - widthHexagon / 2.0),
				(int) Math.round(topPoint[1] + heightHexagon / 4));
		Point topRight = new Point((int) Math.round(topPoint[0] + widthHexagon / 2.0),
				(int) Math.round(topPoint[1] + heightHexagon / 4));
		Point botLeft = new Point((int) Math.round(topPoint[0] - widthHexagon / 2.0),
				(int) Math.round(topPoint[1] + 3 * heightHexagon / 4));
		Point botRight = new Point((int) Math.round(topPoint[0] + widthHexagon / 2.0),
				(int) Math.round(topPoint[1] + 3 * heightHexagon / 4));

		// add Points
		this.addPoint(top.x, top.y);
		this.addPoint(topLeft.x, topLeft.y);
		this.addPoint(botLeft.x, botLeft.y);
		this.addPoint(bot.x, bot.y);
		this.addPoint(botRight.x, botRight.y);
		this.addPoint(topRight.x, topRight.y);
	}

}
