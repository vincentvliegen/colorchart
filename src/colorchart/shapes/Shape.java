package colorchart.shapes;

import java.awt.Color;
import java.awt.Polygon;

public class Shape extends Polygon {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4300718654472033201L;
	private Color borderColor;
	private Color fillColor;

	public Shape(Color borderColor, Color fillColor) {
		super();
		setBorderColor(borderColor);
		setFillColor(fillColor);
	}

	public Shape() {
		this(Color.BLACK, null);
	}

	public Color getFillColor() {
		return fillColor;
	}

	public Color getBorderColor() {
		return borderColor;
	}

	public void setBorderColor(Color borderColor) {
		this.borderColor = borderColor;
	}

	public void setFillColor(Color fillColor) {
		this.fillColor = fillColor;
	}

}
