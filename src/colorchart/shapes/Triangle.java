package colorchart.shapes;

import java.awt.*;

public class Triangle extends Shape {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3912998107394778878L;

	public Triangle(Point point1, Point point2, Point point3, Color borderColor, Color fillColor) {
		super(borderColor, fillColor);
		createTriangle(point1, point2, point3);
	}

	public Triangle(Point point1, Point point2, Point point3) {
		super();
		createTriangle(point1, point2, point3);
	}

	private void createTriangle(Point point1, Point point2, Point point3) {
		addPoint(point1.x, point1.y);
		addPoint(point2.x, point2.y);
		addPoint(point3.x, point3.y);
	}
}
