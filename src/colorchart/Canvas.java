package colorchart;

import java.awt.*;
import javax.swing.*;

public class Canvas {

	public static void main(String[] args) {
		new Canvas();
	}

	public Canvas() {
		JFrame canvas = new JFrame("Cube Drop");
		canvas.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		canvas.setLayout(new BorderLayout());
		canvas.add(new Layers(), BorderLayout.CENTER);
		canvas.pack();
		canvas.setLocationRelativeTo(null);// starts in middle screen
		canvas.setVisible(true);
	}

}
