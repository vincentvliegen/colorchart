package colorchart.colors;

public class ColorScheme {

	private final RGBValue[][] scheme;

	public ColorScheme(int sum, int max) {
		this.scheme = calculateAllColors(sum, max);
	}

	private RGBValue[][] calculateAllColors(int sum, int max) {
		RGBValue[][] pyramid = new RGBValue[Math.min(Math.min(sum, max), 3 * max - sum) + 1][];
		for (int indexRow = 0; indexRow <= Math.min(sum, max); indexRow++) {

			int total = sum;
			int red = Math.min(sum, max) - indexRow;
			total -= red;
			int startGreen = Math.min(total, max);
			int green = startGreen;
			total -= startGreen;
			int startBlue = total;
			int blue = startBlue;

			int rowSize = startGreen - startBlue + 1;
			if (rowSize <= 0) {
				break;
			}
			RGBValue[] row = new RGBValue[rowSize];
			for (int index = 0; index < rowSize; index++) {
				row[index] = new RGBValue(red, green, blue);
				blue++;
				green--;
			}
			pyramid[indexRow] = row;
		}
		return pyramid;
	}

	public RGBValue[][] getScheme() {
		return this.scheme;
	}

	// public static void main(String[] args){
	// ColorScheme col = new ColorScheme(258,255);
	// System.out.println(Arrays.deepToString(col.getScheme()));
	// }

}
