package colorchart.colors;

import java.awt.Color;

public class ColorConversions {

	private ColorConversions() {
		// TODO Auto-generated constructor stub
	}

	public static String ColorToHexRGB(Color color){
		String colorInHex = String.format("#%02x%02x%02x", color.getRed(), color.getGreen(), color.getBlue());
		return colorInHex;
	}
	
	public static String ColorToHSV(Color color){
		String colorInHSV = "";
		colorInHSV += String.format("%.2f", calculateHue(color)) + "\u00B0; ";
		colorInHSV += String.format("%.3f", calculateSaturationV(color)) + "; ";
		colorInHSV += String.format("%.3f", calculateValue(color));
		return colorInHSV;
	}
	
	public static String ColorToHSL(Color color){
		String colorInHSL = "";
		colorInHSL += String.format("%.2f", calculateHue(color)) + "\u00B0; ";
		colorInHSL += String.format("%.3f", calculateSaturationL(color)) + "; ";
		colorInHSL += String.format("%.3f", calculateLightness(color));
		return colorInHSL;
	}
	
	public static String ColorToHSI(Color color){
		String colorInHSI = "";
		colorInHSI += String.format("%.2f", calculateHue(color)) + "\u00B0; ";
		colorInHSI += String.format("%.3f", calculateSaturationI(color)) + "; ";
		colorInHSI += String.format("%.3f", calculateIntensity(color));
		return colorInHSI;
	}
	
	public static double calculateHue(Color color){
		double hue;
		double red = color.getRed();
		double green = color.getGreen();
		double blue = color.getBlue();
		hue = Math.toDegrees(Math.atan2((Math.sqrt(3)*(green-blue)),(2*red-green-blue)));
		if(hue<0){
			hue += 360;
		}
		return hue;
	}

	public static int calculateChroma(Color color){
		int chroma;
		int red = color.getRed();
		int green = color.getGreen();
		int blue = color.getBlue();
		int max = Integer.max(Integer.max(red, green), blue);
		int min = Integer.min(Integer.min(red, green), blue);
		chroma = max - min;
		return chroma;
	}
	
	public static double calculateSaturationV(Color color){
		double saturation = 0;
		double value = calculateValue(color);
		if(value != 0){
			saturation = calculateChroma(color)/value;
		}
		return saturation;
	}
	
	public static double calculateSaturationL(Color color){
		double saturation = 0;
		double lightness = calculateLightness(color);
		if(lightness != 1 && lightness != 0){
			saturation = calculateChroma(color)/(1.0-Math.abs(2*lightness-1));
		}
		return saturation;
	}
	
	public static double calculateSaturationI(Color color){
		double saturation = 0;
		double intensity = calculateIntensity(color);
		if(intensity != 0){
			int red = color.getRed();
			int green = color.getGreen();
			int blue = color.getBlue();
			int min = Integer.min(Integer.min(red, green), blue);
			saturation = 1-min/intensity;
		}

		return saturation;
	}
	
	public static double calculateValue(Color color){
		double value;
		int red = color.getRed();
		int green = color.getGreen();
		int blue = color.getBlue();
		int max = Integer.max(Integer.max(red, green), blue);
		value = max / 255f;
		return value;
	}
	
	public static double calculateLightness(Color color){
		double lightness;
		int red = color.getRed();
		int green = color.getGreen();
		int blue = color.getBlue();
		int max = Integer.max(Integer.max(red, green), blue);
		int min = Integer.min(Integer.min(red, green), blue);
		lightness = (max + min)/(2*255f);
		return lightness;
	}
	
	public static double calculateIntensity(Color color){
		double intensity;
		intensity = (color.getRed() + color.getGreen() + color.getBlue())/(3*255f);
		return intensity;
	}
	
}
