package colorchart.colors;

import java.awt.Color;

public class RGBValue {

	private final int red;
	private final int green;
	private final int blue;

	public RGBValue(int red, int green, int blue) {
		this.red = red;
		this.green = green;
		this.blue = blue;
	}

	public int getRed() {
		return red;
	}

	public int getGreen() {
		return green;
	}

	public int getBlue() {
		return blue;
	}

	public String toString() {
		return "{" + red + ";" + green + ";" + blue + "}";

	}

	public Color colorConversion(int maxColorValue) {
		int red = (int) Math.round(getRed() * 255.0 / maxColorValue);
		int green = (int) Math.round(getGreen() * 255.0 / maxColorValue);
		int blue = (int) Math.round(getBlue() * 255.0 / maxColorValue);
//		System.out.println("red: " + red + ", green: " + green + ", blue " + blue);
		return new Color(red, green, blue);
	}

}
