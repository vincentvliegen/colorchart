package colorchart.components;



import java.awt.Color;
import java.util.ArrayList;

import colorchart.Layers;
import colorchart.colors.*;
import colorchart.shapes.*;

public class ColorPanel extends Panel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -110987186107619704L;
	private int imageNumber;
	private RGBValue[][] listOfColors;
	
	public ColorPanel(int imageNumber, Layers layers) {
		super(layers);
		setImageNumber(imageNumber);
		calculateAllShapes();
		updateImage();
	}

	public void changeImage(int imageNumber) {
		setImageNumber(imageNumber);
		calculateAllShapes();
		updateImage();
	}
	
	protected void calculateAllShapes() {
		getListOfShapes().clear();
		// all colors and their relative positions
		setListOfColors(new ColorScheme(getImageNumber(), getLayers().getMaxColorValue()).getScheme());
		// all positions
		double lengthArray = getListOfColors().length;
		double heightHexagon = Layers.getHeightborder() / lengthArray;

		ArrayList<double[]> allTopPoints = calculateAllTopPoints(lengthArray, heightHexagon);
		Color[] correspondingColors = colorsToSingleArray();

		for (int i = 0; i < allTopPoints.size(); i++) {
			Hexagon hex = new Hexagon(allTopPoints.get(i), heightHexagon, correspondingColors[i],
					correspondingColors[i]);
			getListOfShapes().add(hex);
		}

	}

	public ArrayList<double[]> calculateAllTopPoints(double lengthArray, double heightHexagon) {
		ArrayList<double[]> allTops = new ArrayList<double[]>();
		for (int i = 0; i < lengthArray; i++) {
			RGBValue[] row = getListOfColors()[i];
			int nbOfHex = row.length;
			double heightcorrection;
			if (this.getImageNumber() > getLayers().getMaxColorValue()) {
				heightcorrection = (Layers.getHeightborder() - heightHexagon) / 4;
				if (this.getImageNumber() < 2 * getLayers().getMaxColorValue()) {
					double factor;
					double nbOfNonPyramids = getLayers().getMaxColorValue() - 1;
					double newPanelNb = this.getImageNumber() - getLayers().getMaxColorValue();
					factor = newPanelNb / (nbOfNonPyramids + 1);
					heightcorrection *= factor;
				}
			} else {
				heightcorrection = 0;
			}
			double widthHexagon = heightHexagon * Math.cos(Math.PI / 6.0);
			double startX = Layers.getMargin() + (Layers.getWidthborder() - (nbOfHex - 1) * widthHexagon) / 2.0;
			double y = Layers.getMargin() + i * (3 * heightHexagon / 4) + heightcorrection;
			for (int j = 0; j < nbOfHex; j++) {
				double[] point = new double[] { startX + j * widthHexagon, y };
				allTops.add(point);
			}
		}
		return allTops;
	}

	public Color[] colorsToSingleArray() {
		ArrayList<Color> newList = new ArrayList<Color>();
		int maxColorValue = getLayers().getMaxColorValue();
		for (RGBValue[] i : getListOfColors()) {
			for (RGBValue j : i) {
				Color color = j.colorConversion(maxColorValue);		
				newList.add(color);
			}
		}

		Color[] newArray = new Color[newList.size()];
		newArray = newList.toArray(newArray);
		return newArray;
	}

	
	
	public int getImageNumber() {
		return imageNumber;
	}

	public RGBValue[][] getListOfColors() {
		return listOfColors;
	}

	public void setListOfColors(RGBValue[][] listOfColors) {
		this.listOfColors = listOfColors;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public void setImageNumber(int imageNumber) {
		this.imageNumber = imageNumber;
	}

}
