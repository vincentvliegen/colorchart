package colorchart.components;

import java.awt.Color;
import java.awt.Point;

import colorchart.Layers;
import colorchart.shapes.Hexagon;
import colorchart.shapes.Triangle;

public class BorderPanel extends Panel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final static int triangleMargin = 25;
	private int triangleHeight;
	private int triangleWidth;

	public BorderPanel(Layers layers) {
		super(layers);
		calculateAllShapes();
		updateImage();
	}

	protected void calculateAllShapes() {
		getListOfShapes().clear();
		// hexagon
		double[] top = new double[] { Layers.getMargin() + Layers.getWidthborder() / 2.0, Layers.getMargin() };
		Hexagon hexagon = new Hexagon(top, Layers.getHeightborder());
		getListOfShapes().add(hexagon);
		// Triangles
		double heightGap = getTrianglemargin() * 2 / Math.sqrt(3);
		double heightTriangleDouble = Layers.getHeightborder() / 4.0 - heightGap;
		int heightTriangle = (int) Math.round(heightTriangleDouble);
		int widthTriangle = (int) Math.round(Math.sqrt(3) * heightTriangleDouble);
		setTriangleHeight(heightTriangle);
		setTriangleWidth(widthTriangle);
		Point topLeft;
		Point topRight;
		Point botLeft;
		Point botRight;
		// top left triangle
		topLeft = new Point(Layers.getMargin(), Layers.getMargin());
		topRight = new Point(topLeft.x + widthTriangle, topLeft.y);
		botLeft = new Point(topLeft.x, topLeft.y + heightTriangle);
		Triangle topLeftTriangle = new Triangle(topLeft, botLeft, topRight);
		getListOfShapes().add(topLeftTriangle);
		// top right triangle
		topRight = new Point(Layers.getMargin() + Layers.getWidthborder(), Layers.getMargin());
		topLeft = new Point(topRight.x - widthTriangle, topRight.y);
		botRight = new Point(topRight.x, topRight.y + heightTriangle);
		Triangle topRightTriangle = new Triangle(topRight, topLeft, botRight, Color.BLACK, new Color(200,200,200));
		getListOfShapes().add(topRightTriangle);
//		// bot left triangle
//		botLeft = new Point(Layers.getMargin(), Layers.getMargin() + Layers.getHeightborder());
//		topLeft = new Point(botLeft.x, botLeft.y - heightTriangle);
//		botRight = new Point(botLeft.x + widthTriangle, botLeft.y);
//		Triangle botLeftTriangle = new Triangle(botLeft, botRight, topLeft);
//		getListOfShapes().add(botLeftTriangle);
		// bot right triangle
		botRight = new Point(Layers.getMargin() + Layers.getWidthborder(),
				Layers.getMargin() + Layers.getHeightborder());
		topRight = new Point(botRight.x, botRight.y - heightTriangle);
		botLeft = new Point(botRight.x - widthTriangle, botRight.y);
		Triangle botRightTriangle = new Triangle(botRight, topRight, botLeft);
		getListOfShapes().add(botRightTriangle);

	}

	public void removePointer() {
		Color fillColor = getListOfShapes().get(1).getFillColor();
		calculateAllShapes();
		getListOfShapes().get(1).setFillColor(fillColor);
		updateImage();
	}
	
	
	
	public int getTriangleHeight() {
		return triangleHeight;
	}

	public void setTriangleHeight(int triangleHeight) {
		this.triangleHeight = triangleHeight;
	}

	public int getTriangleWidth() {
		return triangleWidth;
	}

	public void setTriangleWidth(int triangleWidth) {
		this.triangleWidth = triangleWidth;
	}
	
	public static int getTrianglemargin() {
		return triangleMargin;
	}

}
