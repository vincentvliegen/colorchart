package colorchart.components;

import java.awt.BorderLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import colorchart.Layers;

public class TextPanel extends JPanel{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final Layers layers;
	private final JTextField textField;
	private final JLabel label;
	
	public TextPanel(Layers layers, String label){
		this.layers = layers;
		this.textField = new JTextField(5);
		this.label = new JLabel(label);
		this.setOpaque(false);
		this.setLayout(new BorderLayout());
		this.add(getLabel(),BorderLayout.WEST);
		this.add(getTextField(),BorderLayout.EAST);
	}

	public Layers getLayers() {
		return layers;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public JTextField getTextField() {
		return textField;
	}

	public JLabel getLabel() {
		return label;
	}

}
