package colorchart.components.sliders;

import colorchart.Layers;
import colorchart.shapes.Triangle;

public class PrecisionSlider extends TriangularSlider{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public PrecisionSlider(int min, int max, Triangle zone, Layers layers) {
		super(min, max, zone, layers);
	}

}
