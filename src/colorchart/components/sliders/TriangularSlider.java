package colorchart.components.sliders;

import colorchart.Layers;
import colorchart.components.Panel;
import colorchart.shapes.Hexagon;
import colorchart.shapes.Triangle;

public class TriangularSlider extends Panel{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int value;
	private int minValue;
	private int maxValue;
	private Hexagon button;
	private final Triangle zone;
	private final double[] maxTrack;
	private final double[] minTrack;
	private double[] middlePoint;
	private double buttonHeight;
	private static final int minButtonHeight = 25;
	private double ratio;
	
	public TriangularSlider(int min, int max, int value, Triangle zone, Layers layers) {
		super(layers);
		this.zone = zone;
		double[][] track = getTrack();
		this.maxTrack = track[0];
		this.minTrack = track[1];
		this.setMinValue(min);
		this.setMaxValue(max);
		this.setValue(value);
		calculateAllShapes();
		updateImage();
	}
	
	public TriangularSlider(int min, int max, Triangle zone, Layers layers) {
		this(min, max, (min+max)/2, zone, layers);
	}
	
	@Override
	protected void calculateAllShapes() {//TODO track?
		getListOfShapes().clear();
		//zone
		getListOfShapes().add(getZone());
		//button
		calculateButton();
		getListOfShapes().add(getButton());
	}
	
	public void updateButton(int value){
		setValue(value);
		calculateAllShapes();
	}
	
	public void calculateButton(){
		double value = getValue();
		double max = getMaxValue();
		double min = getMinValue();
		double ratio = (value - min)/(max - min);
		setRatio(ratio);
		setMiddlePoint(getPositionOnTrack());
		double height;
		if(getZoneHeight()-getMiddlePoint()[1] < getMiddlePoint()[1]){//top triangle
			height = (getZoneHeight()-getMiddlePoint()[1])*2;
		}else{//bottom triangle
			height = getMiddlePoint()[1]*2;
		}
		setButtonHeight(height);
//		System.out.println(height);
		double[] topPoint = {getMiddlePoint()[0], getMiddlePoint()[1] - getButtonHeight()/2.0};
		setButton(new Hexagon(topPoint, getButtonHeight()));
	}

	public double[][] getTrack(){
		double[][] track = new double[2][];
		double[] maxTrack = new double[2];
		double[] minTrack = new double[2];
		int xRightAngle = getZone().xpoints[0];
		int yRightAngle = getZone().ypoints[0];
		
		//widthHexagon = heightHexagon * Math.cos(Math.PI / 6.0) = height*a;
		//2*(getZoneWidth()-heightHexagon * getZoneWidth()/getZoneHeight()) = widthHexagon
		//heightHexagon * Math.cos(Math.PI / 6.0)/2 = getZoneWidth()-heightHexagon * getZoneWidth()/getZoneHeight();
		//heightHexagon * (Math.cos(Math.PI / 6.0)/2 + getZoneWidth()/getZoneHeight()) = getZoneWidth
		//
		double a = Math.cos(Math.PI / 6.0);
		double b = getZoneWidth()/getZoneHeight();
		double heightMaxHexagon = getZoneWidth()/(a/2+b);
		double lengthMaxSide = heightMaxHexagon*a/2.0;
		double heightMinHexagon = getMinbuttonheight();
		double lengthMinSide = heightMinHexagon*getZoneWidth()/getZoneHeight();
		if(xRightAngle == 0){//leftside max
			if(yRightAngle == 0){//lefttop max
				maxTrack[0] = lengthMaxSide;
				maxTrack[1] = heightMaxHexagon/2;
				minTrack[0] = getZoneWidth()-lengthMinSide;
				minTrack[1] = heightMinHexagon/2;
			}else{//leftbottom max
				maxTrack[0] = lengthMaxSide;
				maxTrack[1] = getZoneHeight()-heightMaxHexagon/2;
				minTrack[0] = getZoneWidth()-lengthMinSide;
				minTrack[1] = getZoneHeight()-heightMinHexagon/2;		
			}
		}else{//rightside max
			if(yRightAngle == 0){//righttop max
				maxTrack[0] = getZoneWidth()-lengthMaxSide;
				maxTrack[1] = heightMaxHexagon/2;
				minTrack[0] = lengthMinSide;
				minTrack[1] = heightMinHexagon/2;
			}else{//rightbottom max
				maxTrack[0] = getZoneWidth()-lengthMaxSide;
				maxTrack[1] = getZoneHeight()-heightMaxHexagon/2;
				minTrack[0] = lengthMinSide;
				minTrack[1] = getZoneHeight()-heightMinHexagon/2;
			}
		}
		track[0] = maxTrack;
		track[1] = minTrack;
		return track;
	}
		
	public double[] getPositionOnTrack(){
		double maxX = getMaxTrack()[0];
		double maxY = getMaxTrack()[1];
		double minX = getMinTrack()[0];
		double minY = getMinTrack()[1];

		double x = minX + getRatio()*(maxX-minX);	
		double y = minY + getRatio()*(maxY-minY);
		double[] point = {x,y};
		return point;
	}
	

	public final double getZoneWidth(){
		return  getZone().getBounds().getWidth();
	}	
	
	public final double getZoneHeight(){
		return  getZone().getBounds().getHeight();
	}	
	
	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

	public int getMinValue() {
		return minValue;
	}

	public void setMinValue(int minValue) {
		this.minValue = minValue;
	}

	public int getMaxValue() {
		return maxValue;
	}

	public void setMaxValue(int maxValue) {
		this.maxValue = maxValue;
	}

	public Hexagon getButton() {
		return button;
	}

	public void setButton(Hexagon button) {
		this.button = button;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	public Triangle getZone() {
		return zone;
	}

	public double getButtonHeight() {
		return buttonHeight;
	}

	public void setButtonHeight(double buttonHeight) {
		this.buttonHeight = buttonHeight;
	}

	public static int getMinbuttonheight() {
		return minButtonHeight;
	}

	public double[] getMiddlePoint() {
		return middlePoint;
	}

	public void setMiddlePoint(double[] middlePoint) {
		this.middlePoint = middlePoint;
	}
	
	public void setMiddlePoint(double x, double y) {
		double[] point = {x,y};
		setMiddlePoint(point);
	}

	public double[] getMaxTrack() {
		return maxTrack;
	}

	public double[] getMinTrack() {
		return minTrack;
	}

	public double getRatio() {
		return ratio;
	}

	public void setRatio(double ratio) {
		this.ratio = ratio;
	}

	
	
}
