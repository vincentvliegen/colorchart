package colorchart.components.sliders;

import java.awt.Color;
import java.awt.GradientPaint;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

import colorchart.Layers;
import colorchart.shapes.Shape;
import colorchart.shapes.Triangle;

public class IntensitySlider extends TriangularSlider{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
	public IntensitySlider(int min, int max, Triangle zone, Layers layers) {
		super(min, max, zone, layers);
	}

	@Override
	public void calculateButton(){
		super.calculateButton();
		getButton().setFillColor(getGrey());
	}
	
	@Override
	public void updateImage(){
		setCurrentImage(new BufferedImage(getLayers().getBorderPanel().getTriangleWidth()+1,getLayers().getBorderPanel().getTriangleHeight()+1,BufferedImage.TYPE_4BYTE_ABGR));
		Graphics2D graphics = getCurrentImage().createGraphics();
		Color borderColor;
		Color fillColor;
		for (Shape s : getListOfShapes()) {
			borderColor = s.getBorderColor();
			fillColor = s.getFillColor();
			if (fillColor != null) {
				graphics.setColor(fillColor);
				graphics.fillPolygon(s);
			}
			if(s == getZone()){
				GradientPaint whiteToBlack = getGradient();
				graphics.setPaint(whiteToBlack);
				graphics.fill(s);
				graphics.setPaint(null);
			}
			graphics.setColor(borderColor);
			graphics.drawPolygon(s);
		}
		graphics.dispose();
		repaint();
	}
	
	public GradientPaint getGradient(){
		return new GradientPaint(getZone().xpoints[0],getZone().ypoints[0],Color.white,getZone().xpoints[1],getZone().ypoints[1],Color.black);
	}
	
	public Color getGrey(){
		int value = (int) (255 * getRatio());
		return new Color(value,value,value);
	}
	
}
