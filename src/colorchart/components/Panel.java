package colorchart.components;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

import javax.swing.JPanel;

import colorchart.Layers;
import colorchart.shapes.Shape;

public abstract class Panel extends JPanel{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected final Layers layers;
	protected BufferedImage currentImage;
	protected final ArrayList<Shape> listOfShapes;
	
	public Panel(Layers layers) {
		this.layers = layers;
		this.listOfShapes = new ArrayList<Shape>();
		this.setOpaque(false);
	}
	
	protected abstract void calculateAllShapes();

	public void updateImage(){
		setCurrentImage(new BufferedImage(Layers.getWidthPanel(),Layers.getHeightPanel(),BufferedImage.TYPE_4BYTE_ABGR));
		Graphics graphics = getCurrentImage().createGraphics();
		Color borderColor;
		Color fillColor;
		for (Shape s : getListOfShapes()) {
			borderColor = s.getBorderColor();
			fillColor = s.getFillColor();
			if (fillColor != null) {
				graphics.setColor(fillColor);
				graphics.fillPolygon(s);
			}
			graphics.setColor(borderColor);
			graphics.drawPolygon(s);
		}
		graphics.dispose();
		repaint();
	}
	
	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.drawImage(getCurrentImage(), 0, 0, this);
		g.dispose();

	}
	
	
	
	public BufferedImage getCurrentImage() {
		return currentImage;
	}

	public void setCurrentImage(BufferedImage currentImage) {
		this.currentImage = currentImage;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Layers getLayers() {
		return layers;
	}

	public ArrayList<Shape> getListOfShapes() {
		return listOfShapes;
	}
	
}
